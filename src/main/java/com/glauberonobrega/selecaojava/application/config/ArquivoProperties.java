package com.glauberonobrega.selecaojava.application.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties
public class ArquivoProperties {

	@Value("${file:upload-dir}")
	private String uploadDiretorio;
}
