package com.glauberonobrega.selecaojava.application.config;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.glauberonobrega.selecaojava.application.service.impl.MessageServiceImpl;
import com.glauberonobrega.selecaojava.domain.dto.ResponseDTO;

@Component
public class ConfigResponse<T> {

	@Autowired
	protected MessageServiceImpl mensagemService;

	public ResponseEntity<ResponseDTO<T>> ok(T obj) {
		return ResponseEntity.ok((ResponseDTO<T>) ResponseDTO.builder().data(obj).build());
	}

	public ResponseEntity<ResponseDTO<T>> create(T obj, String location) {
		return ResponseEntity.created(URI.create(location))
				.body((ResponseDTO<T>) ResponseDTO.builder().data(obj).build());
	}

	public ResponseEntity<ResponseDTO<T>> accepted(T obj) {
		return ResponseEntity.accepted().body((ResponseDTO<T>) ResponseDTO.builder().data(obj).build());
	}

	public ResponseEntity<ResponseDTO<String>> ok(String messageSource) {
		return ResponseEntity.ok(new ResponseDTO<String>(mensagemService.getMessage(messageSource), null));
	}
}
