package com.glauberonobrega.selecaojava.application.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LinhaArquivo {
	
	private String regiao;
	private String estado;
	private String municipio;
	private String revendedor;
	private String codigoProduto;
	private String produto;
	private String dataColeta;
	private String valorCompra;
	private String valorVenda;
	private String unidadeMedida;
	private String bandeira;
	
}

