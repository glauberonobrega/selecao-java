package com.glauberonobrega.selecaojava.application.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.glauberonobrega.selecaojava.domain.enums.Operacao;
import com.glauberonobrega.selecaojava.domain.enums.TipoCombustivel;
import com.glauberonobrega.selecaojava.domain.enums.UnidadeDeMedida;
import com.glauberonobrega.selecaojava.domain.model.HistoricoPreco;
import com.glauberonobrega.selecaojava.domain.model.Revendedor;

public class ArquivoParser {

	
	public static Revendedor converteLinhaToRevendedo(LinhaArquivo linha) {

		return Revendedor.builder()
		.regiao(linha.getRegiao())
		.estado(linha.getEstado())
		.nomeMunicipio(linha.getMunicipio())
		.nomeRevendedor(linha.getRevendedor())
		.build();
	
	}

	
	public static HistoricoPreco converteLinhaToHistorico(LinhaArquivo linha) {
		
		return HistoricoPreco.builder()
		.codigoProduto(linha.getCodigoProduto())
		.produto((TipoCombustivel.recuperaValorPorDescricao(linha.getProduto())))
		.dataColeta(LocalDate.parse(linha.getDataColeta(), DateTimeFormatter.ofPattern("d/MM/yyyy")))
		.valorCompra(new BigDecimal(Util.formatarCampo(Util.validarCampo(linha.getValorCompra()))))
		.valorVenda(new BigDecimal(Util.formatarCampo(Util.validarCampo(linha.getValorVenda()))))
		.unidadeMedida(UnidadeDeMedida.LITRO)
		.tipoOperacao("".equals(linha.getValorCompra()) ? Operacao.VENDA : Operacao.COMPRA)
		.nomeBandeira(linha.getBandeira())
		.build();
	}
	
}
