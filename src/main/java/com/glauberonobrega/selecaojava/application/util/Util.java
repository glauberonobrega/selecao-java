package com.glauberonobrega.selecaojava.application.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.glauberonobrega.selecaojava.infrastructure.log.LogBuilder;

public class Util {

	protected final static Logger log = LoggerFactory.getLogger(Util.class);
	
	public static List<LinhaArquivo> lerArquivo(InputStream arquivo) throws IOException{	
		var br = new BufferedReader(new InputStreamReader(arquivo));
		String linha = "";
		List<LinhaArquivo> linhas = new ArrayList<LinhaArquivo>();
		while ((linha = br.readLine()) != null) {
			String[] conteudo = linha.split("  ");	
			if (conteudo.length != 11) {
				
					log.info(LogBuilder.of()
						.header("Linha mal formatada")
						.row("Linha", linha)
						.build());
			} else {
				if(!conteudo[1].contains("Estado - Sigla")) {
					var linhaConteudo = new LinhaArquivo(
							conteudo[0].trim(), 
							conteudo[1].trim(), 
							conteudo[2], 
							conteudo[3], 
							conteudo[4],
							conteudo[5], 
							conteudo[6], 
							conteudo[7], 
							conteudo[8], 
							conteudo[9], 
							conteudo[10]);
					linhas.add(linhaConteudo);
				}
			}
		}
		return linhas;		
	}
	
	
	public static String validarCampo(String valor) {
		if (valor == null || "".equals(valor)) {
			return "0";
		}
		return valor;
	}
	
	public static String formatarCampo(String valor) {
		if (valor.contains(",")) {
			return valor.replace(",", ".");
		}
		return valor;
	}
}
