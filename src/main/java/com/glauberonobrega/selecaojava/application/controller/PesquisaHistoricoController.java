package com.glauberonobrega.selecaojava.application.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.glauberonobrega.selecaojava.application.config.ConfigResponse;
import com.glauberonobrega.selecaojava.application.service.HistoricoPrecoService;
import com.glauberonobrega.selecaojava.domain.dto.CompraVendaResponseDTO;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorResponseDTO;
import com.glauberonobrega.selecaojava.domain.dto.ResponseDTO;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value="/api", tags = {"Histórico - Pesquisa"}, description = "Operações Histórico")
public class PesquisaHistoricoController {

	@Autowired
	private ConfigResponse configResponse;
	
	@Autowired
	private HistoricoPrecoService historicoPrecoService;
	
	@ApiOperation(
			value = "Valor médio compra/venda por Município",
			notes = "Responsável por recuperar o valor médio de compra/venda por Município",
			response = CompraVendaResponseDTO.class )
	@GetMapping(
			value = "/historicos/pesquisa/compra-venda-municipio/{municipio}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<CompraVendaResponseDTO>> compraVendaPorMunicipio (@PathVariable String municipio) throws RequisicaoInvalidaException {
		return this.configResponse.ok(this.historicoPrecoService.mediaCompraVendaPorMunicipio(municipio));
	}
	
	@ApiOperation(
			value = "Valor médio por Município",
			notes = "Responsável por recuperar o valor médio do combustível por Município",
			response = CompraVendaResponseDTO.class )
	@GetMapping(
			value = "/historicos/pesquisa/valor-medio-municipio/{municipio}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<CompraVendaResponseDTO>> valorMedioPorMunicipio (@PathVariable String municipio) throws RequisicaoInvalidaException {
		return this.configResponse.ok(this.historicoPrecoService.valorMedioPorMunicipio(municipio));
	}
	
	@ApiOperation(
			value = "Valor médio compra/venda por Bandeira",
			notes = "Responsável por recuperar o valor médio de compra/venda por Bandeira",
			response = CompraVendaResponseDTO.class )
	@GetMapping(
			value = "/historicos/pesquisa/compra-venda-bandeira/{bandeira}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<CompraVendaResponseDTO>> compraVendaPorBandeira (@PathVariable String bandeira) throws RequisicaoInvalidaException {
		return this.configResponse.ok(this.historicoPrecoService.mediaCompraVendaPorBandeira(bandeira));
	}
	
	@ApiOperation(
			value = "Histórico por data de coleta",
			notes = "Responsável por retornar os dados agrupados pela data da coleta",
			response = HistoricoRevendedorResponseDTO.class )
	@GetMapping(
			value = "/historicos/pesquisa/data-coleta",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<List<HistoricoRevendedorResponseDTO>>> recuperarDadosPorDataColeta () throws NaoEncontradoException {
		return this.configResponse.ok(this.historicoPrecoService.buscarDadosPorDataColeta());
	}
	
	@ApiOperation(
			value = "Histórico por região",
			notes = "Responsável por retornar os dados agrupados por região",
			response = HistoricoRevendedorResponseDTO.class )
	@GetMapping(
			value = "/historicos/pesquisa/regiao/{regiao}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<List<HistoricoRevendedorResponseDTO>>> recuperarDadosPorRegiao (@PathVariable String regiao) throws RequisicaoInvalidaException, NaoEncontradoException {
		return this.configResponse.ok(this.historicoPrecoService.buscarDadosPorRegiao(regiao));
	}
	
	@ApiOperation(
			value = "Histórico por revendedor",
			notes = "Responsável por retornar os dados agrupados por revendedor",
			response = HistoricoRevendedorResponseDTO.class )
	@GetMapping(
			value = "/historicos/pesquisa/distribuidora/{nome}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<List<HistoricoRevendedorResponseDTO>>> recuperarDadosPorRevendedor (@PathVariable String nome) throws RequisicaoInvalidaException, NaoEncontradoException {
		return this.configResponse.ok(this.historicoPrecoService.buscarDadosPorDistribuidora(nome));
	}
	
}
