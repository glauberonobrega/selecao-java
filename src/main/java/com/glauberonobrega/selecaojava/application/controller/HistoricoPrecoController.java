package com.glauberonobrega.selecaojava.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.glauberonobrega.selecaojava.application.config.ConfigResponse;
import com.glauberonobrega.selecaojava.application.service.HistoricoPrecoService;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorRequestDTO;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorResponseDTO;
import com.glauberonobrega.selecaojava.domain.dto.ResponseDTO;
import com.glauberonobrega.selecaojava.infrastructure.exception.ErroSolicitacaoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value = "/api", tags = { "Histórico - Crud" }, description = "Operações Histórico")
public class HistoricoPrecoController {

	@Autowired
	private ConfigResponse configResponse;

	@Autowired
	private HistoricoPrecoService historicoPrecoService;

	@ApiOperation(value = "Cadastrar Histórico de Preços", notes = "Responsável pelo cadastro de histórico de preço de combustível", response = HistoricoRevendedorResponseDTO.class)
	@PostMapping(value = "/historicos", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<HistoricoRevendedorResponseDTO>> cadastrarHistorico(
			@RequestBody @Valid HistoricoRevendedorRequestDTO request) throws RequisicaoInvalidaException, ErroSolicitacaoException {
		return this.configResponse.create(this.historicoPrecoService.salvar(request), "/historicos");
	}

	@ApiOperation(value = "Atualizar Histórico de Preços", notes = "Responsável por atualizar o histórico de preço de combustível", response = HistoricoRevendedorResponseDTO.class)
	@PutMapping(value = "/historicos/historico/{idHistorico}/revendedor/{idRevendedor}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<HistoricoRevendedorResponseDTO>> atualizarHistorico(
			@PathVariable Long idHistorico, @PathVariable Long idRevendedor,
			@RequestBody @Valid HistoricoRevendedorRequestDTO historico) throws NaoEncontradoException, RequisicaoInvalidaException {
		return this.configResponse.ok(this.historicoPrecoService.atualizar(idHistorico, idRevendedor, historico));
	}

	@ApiOperation(value = "Deletar Histórico de Preços", notes = "Responsável por deletar o histórico de preço de combustível", response = String.class)
	@DeleteMapping(value = "/historicos/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<String>> deletarHistorico(@PathVariable Long id)
			throws RequisicaoInvalidaException, NaoEncontradoException {
		return this.configResponse.ok(this.historicoPrecoService.deletar(id));
	}

	@ApiOperation(value = "Listar Histórico de Preços", notes = "Responsável por listar todos os históricos de preço de combustível", response = HistoricoRevendedorResponseDTO.class)
	@GetMapping(value = "/historicos", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<HistoricoRevendedorResponseDTO>> listarHistorico() throws NaoEncontradoException {
		return this.configResponse.ok(this.historicoPrecoService.listar());
	}
}
