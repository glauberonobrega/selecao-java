package com.glauberonobrega.selecaojava.application.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.glauberonobrega.selecaojava.application.config.ConfigResponse;
import com.glauberonobrega.selecaojava.application.service.UsuarioService;
import com.glauberonobrega.selecaojava.domain.dto.ResponseDTO;
import com.glauberonobrega.selecaojava.domain.dto.UsuarioRequestDTO;
import com.glauberonobrega.selecaojava.domain.dto.UsuarioResponseDTO;
import com.glauberonobrega.selecaojava.infrastructure.exception.EmailJaCadastradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value="/api", tags = {"Usuários"}, description = "Operações Usuário")
public class UsuarioController {
	
	@Autowired
	private ConfigResponse configResponse;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@ApiOperation(
			value = "Cadastrar usuário",
			notes = "Responsável pelo cadastro do usuário",
			response = UsuarioResponseDTO.class )
	@PostMapping(
			value = "/usuarios",
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<UsuarioResponseDTO>> cadastrarUsuario (@RequestBody @Valid UsuarioRequestDTO usuario) throws EmailJaCadastradoException {
		return this.configResponse.create(this.usuarioService.salvar(usuario), "/usuarios");
	}
	
	@ApiOperation(
			value = "Atualizar usuário",
			notes = "Responsável por atualizar os dados do usuário",
			response = UsuarioResponseDTO.class )
	@PutMapping(
			value = "/usuarios/{id}",
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<UsuarioResponseDTO>> atualizarUsuario (@PathVariable Long id, @RequestBody @Valid UsuarioRequestDTO usuario) throws NaoEncontradoException {
		return this.configResponse.ok(this.usuarioService.atualizar(id, usuario));
	}
	
	@ApiOperation(
			value = "Deletar usuário",
			notes = "Responsável por deletar os dados do usuário",
			response = UsuarioResponseDTO.class )
	@DeleteMapping(
			value = "/usuarios/{id}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<UsuarioResponseDTO>> deletarUsuario (@PathVariable Long id) throws NaoEncontradoException {
		return this.configResponse.ok(this.usuarioService.deletar(id));
	}
	
	@ApiOperation(
			value = "Listar usuários",
			notes = "Responsável por listar todos os usuários cadastrados",
			response = UsuarioResponseDTO.class )
	@GetMapping(
			value = "/usuarios",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<UsuarioResponseDTO>> listarUsuarios () throws NaoEncontradoException {
		return this.configResponse.ok(this.usuarioService.listar());
	}
	
	@ApiOperation(
			value = "Buscar usuário por e-mail",
			notes = "Responsável por buscar o usuário cadastrados pelo e-mail",
			response = UsuarioResponseDTO.class )
	@GetMapping(
			value = "/usuarios/{email}",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ResponseDTO<UsuarioResponseDTO>> getUsuario (@PathVariable String email) throws NaoEncontradoException, RequisicaoInvalidaException {
		return this.configResponse.ok(this.usuarioService.buscar(email));
	}
	
}
