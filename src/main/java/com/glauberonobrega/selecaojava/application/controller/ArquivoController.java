package com.glauberonobrega.selecaojava.application.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.glauberonobrega.selecaojava.application.config.ConfigResponse;
import com.glauberonobrega.selecaojava.application.service.impl.ArmazenamentoArquivoServiceImpl;
import com.glauberonobrega.selecaojava.domain.dto.ResponseDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
@Api(value="/api", tags = {"Importação arquivos"}, description = "Operações arquivos")
public class ArquivoController {

	@Autowired
	private ConfigResponse configResponse;
	
	@Autowired
	private ArmazenamentoArquivoServiceImpl armazenamentoArquivoServiceImpl;

	@ApiOperation(
			value = "Importação de arquivo",
			notes = "Responsável pela importação de arquivos",
			response = String.class )
	@PostMapping(value = "/importar-arquivos", consumes = "multipart/form-data")
	public @ResponseBody ResponseEntity<ResponseDTO<String>> uploadArquivo(@RequestPart("file") @Valid @NotNull @NotBlank MultipartFile file) throws Exception {
		return this.configResponse.ok(armazenamentoArquivoServiceImpl.importarArquivo(file));
	}

}
