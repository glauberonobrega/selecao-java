package com.glauberonobrega.selecaojava.application.handler;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.glauberonobrega.selecaojava.application.service.impl.MessageServiceImpl;
import com.glauberonobrega.selecaojava.domain.dto.ResponseDTO;
import com.glauberonobrega.selecaojava.infrastructure.exception.ArmazenamentoArquivoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.ArquivoNaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.BandeiraNaoEncontradaExcpetion;
import com.glauberonobrega.selecaojava.infrastructure.exception.EmailJaCadastradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.ErroSolicitacaoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.MunicipioNaoEncontradoExcpetion;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;
import com.glauberonobrega.selecaojava.infrastructure.log.LogBuilder;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	protected MessageServiceImpl mensagemService;

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@ExceptionHandler({ NaoEncontradoException.class })
	public ResponseEntity<Object> handleInformacaoNaoEncontradaException(NaoEncontradoException ex, WebRequest request) {
		return handleException(ex, HttpStatus.NOT_FOUND, request, "recurso.informacao.nao-encontrada");
	}
	
	@ExceptionHandler({ RequisicaoInvalidaException.class })
	public ResponseEntity<Object> handleRequisicaoInvalidaException(RequisicaoInvalidaException ex, WebRequest request) {
		return handleException(ex, HttpStatus.BAD_REQUEST, request, "recurso.operacao-erro");
	}

	@ExceptionHandler({ ArquivoNaoEncontradoException.class })
	public ResponseEntity<Object> handleArquivoNaoEncontradoException(ArquivoNaoEncontradoException ex, WebRequest request) {
		return handleException(ex, HttpStatus.INTERNAL_SERVER_ERROR, request, "recurso.arquivo.nao-encontrado");
	}

	@ExceptionHandler({ ArmazenamentoArquivoException.class })
	public ResponseEntity<Object> handleArmazenamentoArquivoException(ArmazenamentoArquivoException ex, WebRequest request) {
		return handleException(ex, HttpStatus.INTERNAL_SERVER_ERROR, request, "recurso.armazenamento.erro");
	}
	
	@ExceptionHandler({ ErroSolicitacaoException.class })
	public ResponseEntity<Object> handleErroSolicitacaoExpection(ErroSolicitacaoException ex, WebRequest request) {
		return handleException(ex, HttpStatus.BAD_REQUEST, request, "recurso.operacao-erro");
	}
	
	@ExceptionHandler({ EmailJaCadastradoException.class })
	public ResponseEntity<Object> handleEmailJaCadastradoException(EmailJaCadastradoException ex, WebRequest request) {
		return handleException(ex, HttpStatus.BAD_REQUEST, request, "recurso.email-erro");
	}
	
	@ExceptionHandler({ MunicipioNaoEncontradoExcpetion.class })
	public ResponseEntity<Object> handleMunicipioNaoEncontradoExcpetion(MunicipioNaoEncontradoExcpetion ex, WebRequest request) {
		return handleException(ex, HttpStatus.BAD_REQUEST, request, "recurso.municipio-erro");
	}
	
	@ExceptionHandler({ BandeiraNaoEncontradaExcpetion.class })
	public ResponseEntity<Object> handleBandeiraNaoEncontradoExcpetion(BandeiraNaoEncontradaExcpetion ex, WebRequest request) {
		return handleException(ex, HttpStatus.BAD_REQUEST, request, "recurso.bandeira-erro");
	}
	
	@ExceptionHandler({ RuntimeException.class })
	public ResponseEntity<Object> handleRuntimeException(RuntimeException ex, WebRequest request) {
		return handleException(ex, HttpStatus.INTERNAL_SERVER_ERROR, request, "recurso.operacao-erro");
	}
	
	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
		ResponseDTO<String> responseDTO = new ResponseDTO<>();

		ex.getConstraintViolations().forEach(e -> {
			String msg = MessageFormat.format(e.getMessage(), mensagemService.getMessage(e.getPropertyPath()));
			responseDTO.getErrors().add(msg);
		});
		return handleExceptionInternal(ex, responseDTO, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	protected ResponseEntity<Object> handleException(Exception ex, HttpStatus status, WebRequest request, String chave) {
		ResponseDTO<List<String>> responseDTO = new ResponseDTO<>();
		responseDTO.setErrors(Arrays.asList((mensagemService.getMessage(chave))));
		return handleExceptionInternal(ex, responseDTO, new HttpHeaders(), status, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		ResponseDTO<List<String>> responseDTO = new ResponseDTO<>();
		List<String> erros = obterListaErros(ex.getBindingResult());
		responseDTO.setErrors(erros);
		
		log.error(LogBuilder.of()
				.header("Erro")
				.row("Headers", headers)
				.row("Exception", responseDTO.getErrors())
				.build());
		
		return handleExceptionInternal(ex, responseDTO, headers, HttpStatus.BAD_REQUEST, request);
	}

	protected List<String> obterListaErros(BindingResult bindingResult) {
		List<String> erros = new ArrayList<>();
		bindingResult.getFieldErrors().forEach(e -> erros.add(mensagemService.getMessage(e)));
		return erros;
	}
}
