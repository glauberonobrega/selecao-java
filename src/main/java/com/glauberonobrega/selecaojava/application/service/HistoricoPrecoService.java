package com.glauberonobrega.selecaojava.application.service;

import java.math.BigDecimal;
import java.util.List;

import com.glauberonobrega.selecaojava.domain.dto.CompraVendaResponseDTO;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorRequestDTO;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorResponseDTO;
import com.glauberonobrega.selecaojava.infrastructure.exception.ErroSolicitacaoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;

public interface HistoricoPrecoService {

	HistoricoRevendedorResponseDTO salvar(HistoricoRevendedorRequestDTO historicoRevendedorRequestDTO)
			throws RequisicaoInvalidaException, ErroSolicitacaoException;

	HistoricoRevendedorResponseDTO atualizar(Long idHistorico, Long idRevendedor,
			HistoricoRevendedorRequestDTO historicoRevendedorRequestDTO)
			throws NaoEncontradoException, RequisicaoInvalidaException;

	String deletar(Long id) throws RequisicaoInvalidaException, NaoEncontradoException;

	List<HistoricoRevendedorResponseDTO> listar() throws NaoEncontradoException;

	CompraVendaResponseDTO mediaCompraVendaPorMunicipio(String nomeMunicipio) throws RequisicaoInvalidaException;

	BigDecimal valorMedioPorMunicipio(String nomeMunicipio) throws RequisicaoInvalidaException;

	CompraVendaResponseDTO mediaCompraVendaPorBandeira(String nomeBandeira) throws RequisicaoInvalidaException;

	List<HistoricoRevendedorResponseDTO> buscarDadosPorDataColeta() throws NaoEncontradoException;

	List<HistoricoRevendedorResponseDTO> buscarDadosPorRegiao(String regiao)
			throws RequisicaoInvalidaException, NaoEncontradoException;

	List<HistoricoRevendedorResponseDTO> buscarDadosPorDistribuidora(String nomeDistribuidora)
			throws RequisicaoInvalidaException, NaoEncontradoException;

}
