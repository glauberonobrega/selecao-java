package com.glauberonobrega.selecaojava.application.service.impl;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.glauberonobrega.selecaojava.application.config.ArquivoProperties;
import com.glauberonobrega.selecaojava.application.service.ArmazenamentoArquivoService;
import com.glauberonobrega.selecaojava.application.util.ArquivoParser;
import com.glauberonobrega.selecaojava.application.util.LinhaArquivo;
import com.glauberonobrega.selecaojava.application.util.Util;
import com.glauberonobrega.selecaojava.domain.model.HistoricoPreco;
import com.glauberonobrega.selecaojava.infrastructure.exception.ArmazenamentoArquivoException;
import com.glauberonobrega.selecaojava.infrastructure.log.LogBuilder;
import com.glauberonobrega.selecaojava.infrastructure.repository.RevendedorRepository;

@Service
public class ArmazenamentoArquivoServiceImpl implements ArmazenamentoArquivoService {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
    private final Path localizacaoArquivo;
    
    @Autowired
    private RevendedorRepository revendedorRepository;
    
    @Autowired
    public ArmazenamentoArquivoServiceImpl(ArquivoProperties arquivoProperties) {
    	
        this.localizacaoArquivo = Paths.get(arquivoProperties.getUploadDiretorio()).toAbsolutePath().normalize();
        
        log.info(LogBuilder.of()
				.header("Localização de Arquivo")
				.row("Local", localizacaoArquivo)
				.build());
        try {
            Files.createDirectories(this.localizacaoArquivo);
        } catch (Exception ex) {
        	
        	 log.error(LogBuilder.of()
     				.header("Localização de Arquivo")
     				.row("Erro", ex)
     				.build());
        	
            throw new ArmazenamentoArquivoException();
        }
    }

	@Override
	public String importarArquivo(MultipartFile file) throws Exception {
	
		List<LinhaArquivo> linhas = Util.lerArquivo(file.getInputStream());
		
		log.info(LogBuilder.of()
				.header("Importaçao Arquivo")
				.row("Quantidade de Linhas", linhas.size())
				.build());
		
		linhas.forEach(linha -> {
			var revendedor = revendedorRepository.buscarRevendedorByNomeAndEstado(linha.getRevendedor(), linha.getRegiao(), linha.getEstado(), linha.getMunicipio());
			var historico = new HistoricoPreco();
			if (revendedor == null) {
				revendedor = ArquivoParser.converteLinhaToRevendedo(linha);
				revendedor.setHistoricoPreco(new ArrayList<>());
				historico  = ArquivoParser.converteLinhaToHistorico(linha);
				revendedor.getHistoricoPreco().add(historico);
				historico.setRevendedor(revendedor);
				revendedorRepository.save(revendedor);
			} else {
				historico  = ArquivoParser.converteLinhaToHistorico(linha);
				revendedor.getHistoricoPreco().add(historico);
				historico.setRevendedor(revendedor);
				revendedorRepository.save(revendedor);
			}
		});
		
		return "recurso.operacao-sucesso";		
	}
}