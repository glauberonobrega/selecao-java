package com.glauberonobrega.selecaojava.application.service;

import org.springframework.web.multipart.MultipartFile;

public interface ArmazenamentoArquivoService {

	String importarArquivo(MultipartFile file) throws Exception;
}
