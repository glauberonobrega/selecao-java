package com.glauberonobrega.selecaojava.application.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glauberonobrega.selecaojava.application.service.HistoricoPrecoService;
import com.glauberonobrega.selecaojava.domain.dto.CompraVendaResponseDTO;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorRequestDTO;
import com.glauberonobrega.selecaojava.domain.dto.HistoricoRevendedorResponseDTO;
import com.glauberonobrega.selecaojava.domain.model.HistoricoPreco;
import com.glauberonobrega.selecaojava.domain.model.Revendedor;
import com.glauberonobrega.selecaojava.infrastructure.exception.BandeiraNaoEncontradaExcpetion;
import com.glauberonobrega.selecaojava.infrastructure.exception.ErroSolicitacaoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.MunicipioNaoEncontradoExcpetion;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;
import com.glauberonobrega.selecaojava.infrastructure.log.LogBuilder;
import com.glauberonobrega.selecaojava.infrastructure.repository.HistoricoPrecoRepository;
import com.glauberonobrega.selecaojava.infrastructure.repository.RevendedorRepository;

@Service
public class HistoricoPrecoServiceImpl implements HistoricoPrecoService {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ConverterServiceImpl converterService;

	@Autowired
	private RevendedorRepository revendedorRepository;

	@Autowired
	private HistoricoPrecoRepository historicoPrecoRepository;

	@Override
	public HistoricoRevendedorResponseDTO salvar(HistoricoRevendedorRequestDTO historicoRevendedorRequestDTO)
			throws RequisicaoInvalidaException, ErroSolicitacaoException {

		Optional<String> podeSerCodigo = Optional
				.ofNullable(historicoRevendedorRequestDTO.getRevendedor().getCodigoRevendedor());
		podeSerCodigo.orElseThrow(RequisicaoInvalidaException::new);

		var historico = converterService.convert(historicoRevendedorRequestDTO.getHistorico(), HistoricoPreco.class);
		var revendedor = converterService.convert(historicoRevendedorRequestDTO.getRevendedor(), Revendedor.class);

		Optional<Revendedor> revendedorSalvo = revendedorRepository.findById(revendedor.getCodigoRevendedor());
		HistoricoPreco historicoPrecoSalvo;

		if (revendedorSalvo.isPresent()) {
			revendedorSalvo.get().setHistoricoPreco(new ArrayList<HistoricoPreco>());
			historico.setRevendedor(revendedorSalvo.get());
			historicoPrecoSalvo = historicoPrecoRepository.saveAndFlush(historico);
			revendedorSalvo.get().getHistoricoPreco().add(historicoPrecoSalvo);
			revendedorRepository.save(revendedorSalvo.get());
		} else {
			var r = revendedorRepository.saveAndFlush(revendedor);
			r.setHistoricoPreco(new ArrayList<HistoricoPreco>());
			historico.setRevendedor(r);
			historicoPrecoSalvo = historicoPrecoRepository.saveAndFlush(historico);
			r.getHistoricoPreco().add(historicoPrecoSalvo);
		}

		Optional<HistoricoPreco> historicoOptional = Optional.ofNullable(historicoPrecoSalvo);
		historicoOptional.orElseThrow(ErroSolicitacaoException::new);

		log.info(LogBuilder.of()
				.header("Histórico Preços - Cadastro")
				.row("Histórico", historicoOptional.get())
				.build());

		return converterService.convert(historicoPrecoSalvo, HistoricoRevendedorResponseDTO.class);
	}

	@Override
	public HistoricoRevendedorResponseDTO atualizar(Long idHistorico, Long idRevendedor,
			HistoricoRevendedorRequestDTO historicoRevendedorRequestDTO)
			throws NaoEncontradoException, RequisicaoInvalidaException {

		log.info(LogBuilder.of()
				.header("Histórico Preços - Atualização")
				.row("idHistorico", idHistorico)
				.row("idRevendedor", idRevendedor)
				.row("Histórico", historicoRevendedorRequestDTO).build());

		Optional<Long> podeSerCodigoHistorico = Optional.ofNullable(idHistorico);
		podeSerCodigoHistorico.orElseThrow(RequisicaoInvalidaException::new);

		Optional<Long> podeSerCodigoRemetente = Optional.ofNullable(idRevendedor);
		podeSerCodigoRemetente.orElseThrow(RequisicaoInvalidaException::new);

		var historicoUpdate = converterService.convert(historicoRevendedorRequestDTO.getHistorico(), HistoricoPreco.class);
		var revendedorUpdate = converterService.convert(historicoRevendedorRequestDTO.getRevendedor(), Revendedor.class);

		Optional<HistoricoPreco> historicoSalvo = historicoPrecoRepository.findById(idHistorico);
		historicoSalvo.orElseThrow(NaoEncontradoException::new);

		Optional<Revendedor> revendedorSalvo = revendedorRepository.findById(idRevendedor);
		revendedorSalvo.orElseThrow(NaoEncontradoException::new);

		historicoSalvo.get().setCodigoProduto(historicoUpdate.getCodigoProduto());
		historicoSalvo.get().setNomeBandeira(historicoUpdate.getNomeBandeira());
		historicoSalvo.get().setDataColeta(historicoUpdate.getDataColeta());
		historicoSalvo.get().setValorCompra(historicoUpdate.getValorCompra());
		historicoSalvo.get().setValorVenda(historicoUpdate.getValorVenda());
		revendedorSalvo.get().setRegiao(revendedorUpdate.getRegiao());
		revendedorSalvo.get().setEstado(revendedorUpdate.getEstado());
		revendedorSalvo.get().setNomeMunicipio(revendedorUpdate.getNomeMunicipio());
		revendedorSalvo.get().setNomeRevendedor(revendedorUpdate.getNomeRevendedor());
		revendedorSalvo.get().setCodigoRevendedor(revendedorUpdate.getCodigoRevendedor());
		historicoSalvo.get().setRevendedor(revendedorSalvo.get());

		return converterService.convert(historicoPrecoRepository.saveAndFlush(historicoSalvo.get()),
				HistoricoRevendedorResponseDTO.class);
	}

	@Override
	public String deletar(Long id) throws RequisicaoInvalidaException, NaoEncontradoException {

		Optional<Long> podeSerCodigo = Optional.ofNullable(id);
		podeSerCodigo.orElseThrow(RequisicaoInvalidaException::new);

		log.info(LogBuilder.of()
				.header("Histórico Preços - Deleção")
				.row("Id", id).build());

		return historicoPrecoRepository.findById(id).map(historico -> {
			historicoPrecoRepository.deleteById(id);
			revendedorRepository.deleteById(historico.getRevendedor().getIdRevendedor());
			return "recurso.operacao-sucesso";
		}).orElseThrow(NaoEncontradoException::new);
	}

	@Override
	public List<HistoricoRevendedorResponseDTO> listar() throws NaoEncontradoException {
		return buscarDadosPorDataColeta();
	}

	@Override
	public List<HistoricoRevendedorResponseDTO> buscarDadosPorDataColeta() throws NaoEncontradoException {
		List<HistoricoPreco> lista = historicoPrecoRepository.buscarHistoricoPrecoPorDataColeta();
		if (lista.size() == 0) throw new NaoEncontradoException();
		return converterService.convert(lista, HistoricoRevendedorResponseDTO.class);
	}

	@Override
	public List<HistoricoRevendedorResponseDTO> buscarDadosPorRegiao(String regiao)
			throws RequisicaoInvalidaException, NaoEncontradoException {

		Optional<String> podeSerRegiao = Optional.ofNullable(regiao);
		podeSerRegiao.orElseThrow(RequisicaoInvalidaException::new);

		log.info(LogBuilder.of()
				.header("Histórico por Região")
				.row("Região", regiao)
				.build());

		List<HistoricoPreco> lista = historicoPrecoRepository.buscarHistoricoPrecoPorRegiao(regiao);
		if (lista.size() == 0) throw new NaoEncontradoException();
		return converterService.convert(lista, HistoricoRevendedorResponseDTO.class);
	}

	@Override
	public List<HistoricoRevendedorResponseDTO> buscarDadosPorDistribuidora(String nomeDistribuidora)
			throws RequisicaoInvalidaException, NaoEncontradoException {

		Optional<String> podeSerDistribuidora = Optional.ofNullable(nomeDistribuidora);
		podeSerDistribuidora.orElseThrow(RequisicaoInvalidaException::new);

		log.info(LogBuilder.of()
				.header("Histórico por revendedor")
				.row("distribuidora", nomeDistribuidora)
				.build());

		List<HistoricoPreco> lista = historicoPrecoRepository.buscarHistoricoPrecoPorDistribuidora(nomeDistribuidora);
		if (lista.size() == 0) throw new NaoEncontradoException();
		return converterService.convert(lista, HistoricoRevendedorResponseDTO.class);
	}

	@Override
	public CompraVendaResponseDTO mediaCompraVendaPorMunicipio(String nomeMunicipio) throws RequisicaoInvalidaException {

		Optional<String> podeSerMunicipio = Optional.ofNullable(nomeMunicipio);
		podeSerMunicipio.orElseThrow(RequisicaoInvalidaException::new);

		Optional<Long> validaMunicipio = historicoPrecoRepository.validaMunicipio(nomeMunicipio);
		if (validaMunicipio.isPresent() && validaMunicipio.get() == 0) throw new MunicipioNaoEncontradoExcpetion();

		BigDecimal compra = historicoPrecoRepository.recuperarValorMedioCompraPorMunicipio(nomeMunicipio).setScale(2,
				RoundingMode.HALF_UP);
		BigDecimal venda = historicoPrecoRepository.recuperarValorMedioVendaPorMunicipio(nomeMunicipio).setScale(2,
				RoundingMode.HALF_UP);

		log.info(LogBuilder.of()
				.header("Média compra/venda por Município ")
				.row("Compra", compra)
				.row("Venda", venda)
				.build());
		
		return CompraVendaResponseDTO.builder().valorMedioCompra(compra).valorMedioVenda(venda).build();
	}

	@Override
	public CompraVendaResponseDTO mediaCompraVendaPorBandeira(String nomeBandeira) throws RequisicaoInvalidaException {

		Optional<String> podeSerBandeira = Optional.ofNullable(nomeBandeira);
		podeSerBandeira.orElseThrow(RequisicaoInvalidaException::new);

		Optional<Long> validaBandeira = historicoPrecoRepository.validaBandeira(nomeBandeira);
		if (validaBandeira.isPresent() && validaBandeira.get() == 0)
			throw new BandeiraNaoEncontradaExcpetion();

		BigDecimal compra = historicoPrecoRepository.recuperarValorMedioCompraPorBandeira(nomeBandeira).setScale(2,
				RoundingMode.HALF_UP);
		BigDecimal venda = historicoPrecoRepository.recuperarValorMedioVendaPorBandeira(nomeBandeira).setScale(2,
				RoundingMode.HALF_UP);

		log.info(LogBuilder.of()
				.header("Média compra/venda por Bandeira ")
				.row("Compra", compra)
				.row("Venda", venda)
				.build());
		
		return CompraVendaResponseDTO.builder().valorMedioCompra(compra).valorMedioVenda(venda).build();
	}

	@Override
	public BigDecimal valorMedioPorMunicipio(String nomeMunicipio) throws RequisicaoInvalidaException {
		Optional<String> podeSerMunicipio = Optional.ofNullable(nomeMunicipio);
		podeSerMunicipio.orElseThrow(RequisicaoInvalidaException::new);

		Optional<Long> validaMunicipio = historicoPrecoRepository.validaMunicipio(nomeMunicipio);
		if (validaMunicipio.isPresent() && validaMunicipio.get() == 0) throw new MunicipioNaoEncontradoExcpetion();

		return historicoPrecoRepository.recuperarValorMedioVendaPorMunicipio(nomeMunicipio).setScale(2,
				RoundingMode.HALF_UP);
	}

}
