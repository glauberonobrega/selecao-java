package com.glauberonobrega.selecaojava.application.service;

import java.util.List;

import com.glauberonobrega.selecaojava.domain.dto.UsuarioRequestDTO;
import com.glauberonobrega.selecaojava.domain.dto.UsuarioResponseDTO;
import com.glauberonobrega.selecaojava.infrastructure.exception.EmailJaCadastradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;

public interface UsuarioService {

	UsuarioResponseDTO salvar(UsuarioRequestDTO usuario) throws EmailJaCadastradoException;

	UsuarioResponseDTO atualizar(Long id, UsuarioRequestDTO usuario) throws NaoEncontradoException;

	String deletar(Long id) throws NaoEncontradoException;

	List<UsuarioResponseDTO> listar() throws NaoEncontradoException;
	
	UsuarioResponseDTO buscar(String email) throws NaoEncontradoException, RequisicaoInvalidaException;
}
