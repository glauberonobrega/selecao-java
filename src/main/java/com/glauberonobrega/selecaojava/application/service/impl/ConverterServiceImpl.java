package com.glauberonobrega.selecaojava.application.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConverterServiceImpl {

	@Autowired
	private ModelMapper mapper;

	public <T> T convert(Object data, Class<T> type) {
		T target = mapper.map(data, type);
		return target;
	}

	public <T> List<T> convert(List<?> dataList, Class<T> type) {
		return dataList.stream().map(d -> convert(d, type)).collect(Collectors.toList());
	}

}
