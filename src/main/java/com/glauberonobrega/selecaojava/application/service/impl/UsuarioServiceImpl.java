package com.glauberonobrega.selecaojava.application.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glauberonobrega.selecaojava.application.service.UsuarioService;
import com.glauberonobrega.selecaojava.domain.dto.UsuarioRequestDTO;
import com.glauberonobrega.selecaojava.domain.dto.UsuarioResponseDTO;
import com.glauberonobrega.selecaojava.domain.model.Usuario;
import com.glauberonobrega.selecaojava.infrastructure.exception.EmailJaCadastradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.NaoEncontradoException;
import com.glauberonobrega.selecaojava.infrastructure.exception.RequisicaoInvalidaException;
import com.glauberonobrega.selecaojava.infrastructure.log.LogBuilder;
import com.glauberonobrega.selecaojava.infrastructure.repository.UsuarioRespository;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UsuarioRespository usuarioRepository;

	@Autowired
	private ConverterServiceImpl converterService;

	@Override
	public UsuarioResponseDTO salvar(UsuarioRequestDTO usuarioDTO) throws EmailJaCadastradoException {

		Optional<Usuario> podeJaSerCadastrado = Optional.ofNullable(usuarioRepository.findByEmail(usuarioDTO.getEmail()));

		log.info(LogBuilder.of()
				.header("Cadastro Usuário")
				.row("Usuario", usuarioDTO)
				.build());

		podeJaSerCadastrado.ifPresent(u -> {
			throw new EmailJaCadastradoException();
		});

		var usuario = usuarioRepository.save(converterService.convert(usuarioDTO, Usuario.class));
		return converterService.convert(usuario, UsuarioResponseDTO.class);
	}

	@Override
	public UsuarioResponseDTO atualizar(Long id, UsuarioRequestDTO usuarioDTO) throws NaoEncontradoException {

		log.info(LogBuilder.of()
				.header("Atualização Usuário")
				.row("Id", id)
				.row("Usuario", usuarioDTO)
				.build());

		var usuarioUpdated = usuarioRepository.findById(id).map(usuario -> {
			usuario.setNome(usuarioDTO.getNome());
			usuario.setEmail(usuarioDTO.getEmail());
			usuario.setSenha(usuarioDTO.getSenha());
			Usuario updated = usuarioRepository.save(usuario);
			return updated;
		}).orElseThrow(NaoEncontradoException::new);

		return converterService.convert(usuarioUpdated, UsuarioResponseDTO.class);
	}

	@Override
	public String deletar(Long id) throws NaoEncontradoException {

		log.info(LogBuilder.of()
				.header("Deleção Usuário")
				.row("Id", id)
				.build());

		return usuarioRepository.findById(id).map(usuario -> {
			usuarioRepository.deleteById(id);
			return "recurso.operacao-sucesso";
		}).orElseThrow(NaoEncontradoException::new);
	}

	@Override
	public List<UsuarioResponseDTO> listar() throws NaoEncontradoException {
		List<Usuario> lista = usuarioRepository.findAll();
		if (lista.size() == 0) throw new NaoEncontradoException();
		return converterService.convert(lista, UsuarioResponseDTO.class);
	}

	@Override
	public UsuarioResponseDTO buscar(String email) throws NaoEncontradoException, RequisicaoInvalidaException {
		
		log.info(LogBuilder.of()
				.header("Busca por e-mail")
				.row("e-mail", email)
				.build());
		
		Optional<String> podeSerEmail = Optional.ofNullable(email);
		podeSerEmail.orElseThrow(RequisicaoInvalidaException::new);
		
		Optional<Usuario> podeJaSerCadastrado = Optional.ofNullable(usuarioRepository.findByEmail(email));
		podeJaSerCadastrado.orElseThrow(NaoEncontradoException::new);
		
		return converterService.convert(podeJaSerCadastrado.get(), UsuarioResponseDTO.class);
	}

}
