package com.glauberonobrega.selecaojava.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Operacao {

	COMPRA("C", "COMPRA"), 
	VENDA("V", "VENDA");
	
	private String tipo;
	private String descricao;

}
