package com.glauberonobrega.selecaojava.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoCombustivel {
	
	GASOLINA(1, "GASOLINA"), 
	ETANOL(2, "ETANOL"), 	
	DIESEL(3, "DIESEL"),
	GNV(4,"GNV");

	private int cod;
	private String descricao;

	public static TipoCombustivel recuperaValorPorDescricao(String descricao) {		
		for (TipoCombustivel combustivel : TipoCombustivel.values()) {
			if(combustivel.getDescricao().equals(descricao)) {
				return combustivel;
			}
		}
		return null;
	}
}

