package com.glauberonobrega.selecaojava.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UnidadeDeMedida {
	
	LITRO("L", "LITRO");

	private String cod;
	private String descricao;

}
