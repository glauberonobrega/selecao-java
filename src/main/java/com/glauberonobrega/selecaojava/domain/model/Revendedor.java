package com.glauberonobrega.selecaojava.domain.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString

@Entity
@Table(name = "T_REVENDEDOR")
public class Revendedor {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID_REVENDEDOR")
	private Long idRevendedor;
	private String regiao;
	private String estado;
	private String nomeMunicipio;
	private String nomeRevendedor;
	@Transient
	private Long codigoRevendedor;

	@OneToMany(mappedBy = "revendedor", 
			targetEntity = HistoricoPreco.class, 
			fetch = FetchType.LAZY, 
			cascade = CascadeType.ALL)
	private List<HistoricoPreco> historicoPreco;

}
