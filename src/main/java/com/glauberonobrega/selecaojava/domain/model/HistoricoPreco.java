package com.glauberonobrega.selecaojava.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.glauberonobrega.selecaojava.domain.enums.Operacao;
import com.glauberonobrega.selecaojava.domain.enums.TipoCombustivel;
import com.glauberonobrega.selecaojava.domain.enums.UnidadeDeMedida;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "T_HISTORICO_PRECO")
public class HistoricoPreco {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name = "ID_HISTORICO_PRECO")
	private Long idHistoricoPreco;
	private String codigoProduto;
	private String nomeBandeira;
	private LocalDate dataColeta;
	private BigDecimal valorCompra;
	private BigDecimal valorVenda;
	
	@Enumerated(EnumType.STRING)
	private TipoCombustivel produto;
	
	@Enumerated(EnumType.STRING)
	private UnidadeDeMedida unidadeMedida;

	@Enumerated(EnumType.STRING)
	private Operacao tipoOperacao;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "ID_REVENDEDOR")
	private Revendedor revendedor;

}
