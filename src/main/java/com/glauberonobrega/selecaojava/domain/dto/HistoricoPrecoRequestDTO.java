package com.glauberonobrega.selecaojava.domain.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;

import com.glauberonobrega.selecaojava.domain.enums.Operacao;
import com.glauberonobrega.selecaojava.domain.enums.TipoCombustivel;
import com.glauberonobrega.selecaojava.domain.enums.UnidadeDeMedida;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HistoricoPrecoRequestDTO {
	
	@NotEmpty
	private String codigoProduto;
	private TipoCombustivel produto;
	private LocalDate dataColeta;
	private String valorCompra;
	private String valorVenda;
	@NotEmpty
	private String nomeBandeira;
	@NotEmpty
	private String codigoHistorico;
	private Operacao tipoOperacao;
	private UnidadeDeMedida unidadeMedida;

}
