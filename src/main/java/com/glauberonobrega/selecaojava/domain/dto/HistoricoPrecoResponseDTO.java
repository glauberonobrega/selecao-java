package com.glauberonobrega.selecaojava.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.glauberonobrega.selecaojava.domain.enums.TipoCombustivel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HistoricoPrecoResponseDTO {
	
	private Long idHistoricoPreco;
	private String codigoProduto;
	private TipoCombustivel produto;
	private String dataColeta;
	private String valorCompra;
	private String valorVenda;
	private String nomeBandeira;
	private String codigoHistorico;

}
