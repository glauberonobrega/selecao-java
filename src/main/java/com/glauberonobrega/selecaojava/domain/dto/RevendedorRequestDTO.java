package com.glauberonobrega.selecaojava.domain.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RevendedorRequestDTO {

	@NotEmpty
	private String regiao;
	
	@NotEmpty
	private String estado;
	
	@NotEmpty
	private String nomeMunicipio;
	
	@NotEmpty
	private String nomeRevendedor;
	
	@NotEmpty
	private String codigoRevendedor;
}
