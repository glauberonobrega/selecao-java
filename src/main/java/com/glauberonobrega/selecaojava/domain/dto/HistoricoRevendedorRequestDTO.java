package com.glauberonobrega.selecaojava.domain.dto;

import javax.validation.Valid;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HistoricoRevendedorRequestDTO {

	@Valid
	private HistoricoPrecoRequestDTO historico;
	@Valid
	private RevendedorRequestDTO revendedor;
}
