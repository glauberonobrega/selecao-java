package com.glauberonobrega.selecaojava.domain.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioRequestDTO {

	@NotEmpty
	private String nome;
	
	@Email
	private String email;
	
	@NotEmpty
	@Size(min = 8)
	private String senha;
	
}
