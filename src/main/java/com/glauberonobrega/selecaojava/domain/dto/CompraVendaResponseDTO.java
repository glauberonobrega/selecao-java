package com.glauberonobrega.selecaojava.domain.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompraVendaResponseDTO {

	private BigDecimal valorMedioCompra;
	private BigDecimal valorMedioVenda;
}
