package com.glauberonobrega.selecaojava.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RevendedorResponseDTO {
	
	private Long idRevendedor;
	private String regiao;
	private String estado;
	private String nomeMunicipio;
	private String nomeRevendedor;
	private String codigoRevendedor;
	
}
