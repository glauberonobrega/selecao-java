package com.glauberonobrega.selecaojava.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HistoricoRevendedorResponseDTO {

	private HistoricoPrecoResponseDTO historico;
	private RevendedorResponseDTO revendedor;
}
