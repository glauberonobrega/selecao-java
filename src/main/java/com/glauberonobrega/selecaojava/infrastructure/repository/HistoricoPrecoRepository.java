package com.glauberonobrega.selecaojava.infrastructure.repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.glauberonobrega.selecaojava.domain.model.HistoricoPreco;

public interface HistoricoPrecoRepository extends JpaRepository<HistoricoPreco, Long> {

	@Query(value = "SELECT NVL(SUM(H.VALOR_COMPRA)/ COUNT(H.*),0) " + 
			"FROM T_HISTORICO_PRECO H " + 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE  R.NOME_MUNICIPIO = :nomeMunicipio ", nativeQuery=true)
	BigDecimal recuperarValorMedioCompraPorMunicipio(@Param("nomeMunicipio") String nomeMunicipio);
	
	@Query(value = "SELECT COUNT(*) " + 
			"FROM T_HISTORICO_PRECO H " + 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE  R.NOME_MUNICIPIO = :nomeMunicipio ", nativeQuery=true)
	Optional<Long> validaMunicipio(@Param("nomeMunicipio") String nomeMunicipio);
	
	@Query(value = "SELECT NVL(SUM(H.VALOR_VENDA)/ COUNT(H.*),0) " + 
			"FROM T_HISTORICO_PRECO H " + 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE R.NOME_MUNICIPIO = :nomeMunicipio ", nativeQuery=true)
	BigDecimal recuperarValorMedioVendaPorMunicipio(@Param("nomeMunicipio") String nomeMunicipio);
	
	@Query(value = "SELECT NVL(SUM(H.VALOR_COMPRA) / COUNT(H.*),0) " + 
			"FROM T_HISTORICO_PRECO H " + 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE  H.NOME_BANDEIRA = :nomeBandeira ", nativeQuery=true)
	BigDecimal recuperarValorMedioCompraPorBandeira(@Param("nomeBandeira") String nomeBandeira);
	
	@Query(value = "SELECT COUNT(*) " + 
			"FROM T_HISTORICO_PRECO H " + 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE  H.NOME_BANDEIRA = :nomeBandeira ", nativeQuery=true)
	Optional<Long> validaBandeira(@Param("nomeBandeira") String nomeBandeira);
	
	@Query(value = "SELECT NVL(SUM(H.VALOR_VENDA)/ COUNT(H.*),0) " + 
			"FROM T_HISTORICO_PRECO H " + 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE  H.NOME_BANDEIRA = :nomeBandeira ", nativeQuery=true)
	BigDecimal recuperarValorMedioVendaPorBandeira(@Param("nomeBandeira") String nomeBandeira);
	
	@Query(value = "SELECT * " + 
			"FROM T_HISTORICO_PRECO H "+ 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR "+
			"ORDER BY H.DATA_COLETA DESC ", nativeQuery = true)
	List<HistoricoPreco> buscarHistoricoPrecoPorDataColeta();
	
	@Query(value = "SELECT * " + 
			"FROM T_HISTORICO_PRECO H  "+ 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE R.REGIAO = :regiao ", nativeQuery = true)
	List<HistoricoPreco> buscarHistoricoPrecoPorRegiao(@Param("regiao") String regiao);
	
	@Query(value = "SELECT * " + 
			"FROM T_HISTORICO_PRECO H  "+ 
			"INNER JOIN T_REVENDEDOR R ON R.ID_REVENDEDOR = H.ID_REVENDEDOR " +
			"WHERE H.NOME_BANDEIRA = :nomeBandeira ", nativeQuery = true)
	List<HistoricoPreco> buscarHistoricoPrecoPorDistribuidora(@Param("nomeBandeira") String nnomeBandeira);
	
}
