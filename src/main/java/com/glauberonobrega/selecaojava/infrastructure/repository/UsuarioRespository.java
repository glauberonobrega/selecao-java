package com.glauberonobrega.selecaojava.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.glauberonobrega.selecaojava.domain.model.Usuario;

public interface UsuarioRespository extends JpaRepository<Usuario, Long> {
	
	Usuario findByEmail (String email);
}
