package com.glauberonobrega.selecaojava.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.glauberonobrega.selecaojava.domain.model.Revendedor;

public interface RevendedorRepository extends JpaRepository<Revendedor, Long>  {

	Revendedor findByNomeRevendedor(String NomeRevendedor);

	@Query(value = "SELECT * " 
			+ "FROM T_REVENDEDOR R "
			+ "WHERE R.NOME_REVENDEDOR  = :nomeRevendedor  "
			+ "AND R.REGIAO = :regiao "
			+ "AND R.ESTADO = :estado "
			+ "AND R.NOME_MUNICIPIO = :nomeMunicipio ", nativeQuery = true)
	Revendedor buscarRevendedorByNomeAndEstado(
			@Param("nomeRevendedor") String nomeRevendedor,
			@Param("regiao") String regiao, 
			@Param("estado") String estado,
			@Param("nomeMunicipio") String nomeMunicipio);
}
