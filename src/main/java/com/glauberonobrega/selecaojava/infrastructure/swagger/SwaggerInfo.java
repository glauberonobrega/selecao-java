package com.glauberonobrega.selecaojava.infrastructure.swagger;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("swagger-info")
public class SwaggerInfo {

	private String titulo;
	private String descricao;
	private String versao;
	
}
