package com.glauberonobrega.selecaojava.infrastructure.swagger;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class ConfigSwagger {

	@Autowired
	private SwaggerInfo swaggerInfo;
	
	@Bean
	public Docket api() {

		List<ResponseMessage> responseMessages = new ArrayList<ResponseMessage>();
		responseMessages.add(new ResponseMessageBuilder().code(400).message("Requisição inválida.").build());
		responseMessages.add(new ResponseMessageBuilder().code(401).message("Não autorizado.").build());
		responseMessages.add(new ResponseMessageBuilder().code(403).message("Não permitido.").build());
		responseMessages.add(new ResponseMessageBuilder().code(404).message("Recurso não encontrado.").build());
		responseMessages.add(new ResponseMessageBuilder().code(500).message("Erro interno do sistema.").responseModel(new ModelRef("string")).build());

		var docket = new Docket(DocumentationType.SWAGGER_2)
				.genericModelSubstitutes(ResponseEntity.class)
				.directModelSubstitute(java.time.LocalDate.class, java.sql.Date.class)
				.directModelSubstitute(java.time.LocalDateTime.class, java.util.Date.class)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.paths(PathSelectors.any())
				.build()
				.useDefaultResponseMessages(false).globalResponseMessage(RequestMethod.GET, responseMessages)
				.globalResponseMessage(RequestMethod.POST, responseMessages)
				.globalResponseMessage(RequestMethod.PATCH, responseMessages)
				.globalResponseMessage(RequestMethod.PUT, responseMessages)
				.apiInfo(apiInfo());
		
		return docket;
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title(this.swaggerInfo.getTitulo())
				.description(this.swaggerInfo.getDescricao())
				.version(this.swaggerInfo.getVersao())
				.contact(this.contato())
				.build();
	}
	
	private Contact contato() {
		return new Contact(
				"Glauber Oliveira Nóbrega",
				"https://www.linkedin.com/in/glaubenobrega/", 
				"glauberonobrega@gmail.com");
	}
}
