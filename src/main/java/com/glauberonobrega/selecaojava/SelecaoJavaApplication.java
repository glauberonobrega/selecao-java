package com.glauberonobrega.selecaojava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.glauberonobrega.selecaojava.application.config.ArquivoProperties;
import com.glauberonobrega.selecaojava.infrastructure.swagger.SwaggerInfo;

@SpringBootApplication
@EnableAutoConfiguration
@EnableConfigurationProperties({ SwaggerInfo.class , ArquivoProperties.class})
public class SelecaoJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SelecaoJavaApplication.class, args);
	}

}
